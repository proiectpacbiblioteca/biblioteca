#include "ProfileState.h"

json SaveUserIdAndEventIdInJson(const std::string& userId, const std::string& eventId)
{
	json jsonMessage;
	jsonMessage["event"] = eventId;
	jsonMessage["data1"] = userId;
	return jsonMessage;
}

json SaveUserIdBookIdAndEventIdInJson(const std::string& userId, const std::string& bookId, const std::string& eventId)
{
	json jsonMessage;
	jsonMessage["event"] = eventId;
	jsonMessage["data1"] = userId;
	jsonMessage["data2"] = bookId;
	return jsonMessage;
}

ProfileState::ProfileState(sf::RenderWindow* window) 
{
	this->InitVariables();
	this->InitWindow();
	this->InitVisuals();
	this->InitText();
	this->InitButtons();
}

void ProfileState::PollEvents()
{
	while (this->window->pollEvent(this->event))
	{
		switch (this->event.type)
		{
		case sf::Event::Closed:
			this->window->close();
			break;
		case sf::Event::MouseMoved:
			if (mainMenuButton.IsMouseOver(*this->window))
			{
				mainMenuButton.SetBackColor(sf::Color(230, 230, 230));
			}
			else
			{
				mainMenuButton.SetBackColor(sf::Color::White);
			}
			if (returnBookButton.IsMouseOver(*this->window))
			{
				returnBookButton.SetBackColor(sf::Color(230, 230, 230));
			}
			else
			{
				returnBookButton.SetBackColor(sf::Color::White);
			}
			if (extendLoanButton.IsMouseOver(*this->window))
			{
				extendLoanButton.SetBackColor(sf::Color(230, 230, 230));
			}
			else
			{
				extendLoanButton.SetBackColor(sf::Color::White);
			}
			break;
		case sf::Event::MouseButtonPressed:
			if (event.mouseButton.button == sf::Mouse::Left)
			{
				if (mainMenuButton.IsMouseOver(*this->window))
				{
					mainMenuButton.SetBackColor(sf::Color(210, 210, 210));
					mainMenuButtonPressed = true;
					eventProduced = true;
				}
				else
				{
					mainMenuButtonPressed = false;
					eventProduced = false;
				}

				if (returnBookButton.IsMouseOver(*this->window) && returnedBookIndex != -1 && returnedBookIndex < borrowedBooks.size())
				{
					returnBookButton.SetBackColor(sf::Color(210, 210, 210));
					returnButtonPressed = true;

					messageSent = SaveUserIdBookIdAndEventIdInJson(userId, borrowedBooks[returnedBookIndex].GetBookId(), "10").dump();
					connectSocket->Send(messageSent.c_str(), messageSent.size());

					connectSocket->Receive(receivedBuffer.data(), receivedBuffer.size(), received);
					borrowedBooks.erase(borrowedBooks.begin() + returnedBookIndex);
				}
				else
				{
					returnButtonPressed = false;
				}

				if (extendLoanButton.IsMouseOver(*this->window) && returnedBookIndex != -1 && returnedBookIndex < borrowedBooks.size())
				{
					extendLoanButton.SetBackColor(sf::Color(210, 210, 210));

					messageSent = SaveUserIdBookIdAndEventIdInJson(userId, borrowedBooks[returnedBookIndex].GetBookId(), "11").dump();
					connectSocket->Send(messageSent.c_str(), messageSent.size());

					connectSocket->Receive(receivedBuffer.data(), receivedBuffer.size(), received);
				}

				for (int i = 0; i < 5; i++)
				{
					if (borrowedBooksButtons[i].IsMouseOver(*this->window))
					{
						borrowedBooksButtons[i].SetBackColor(sf::Color(210, 210, 210));
						returnedBookIndex = i;
					}
					else
					{
						borrowedBooksButtons[i].SetBackColor(sf::Color::White);
					}
				}
			}
			break;
		}
	}
}

void ProfileState::InitVariables()
{
	returnButtonPressed = false;
	mainMenuButtonPressed = false;
	borrowedBooksButtons.resize(5);
}

void ProfileState::InitWindow()
{
	this->videoMode.width = 900;
	this->videoMode.height = 900;

	this->window = new sf::RenderWindow(this->videoMode, "Pagina Profil", sf::Style::Close);
	this->window->setFramerateLimit(60);
	this->window->setPosition(sf::Vector2i(510,45));
}

void ProfileState::InitText()
{
	if (!fontAllura.loadFromFile("Fonts/Allura-Regular.ttf"))
	{
		std::cout << "Could not load font from file." << std::endl;
		this->window->close();
	}
	if (!fontArial.loadFromFile("Fonts/Arial.ttf"))
	{
		std::cout << "Could not load font from file." << std::endl;
		this->window->close();
	}

	pageTitle = sf::Text("Profile Page", fontAllura, 55);
	pageTitle.setFillColor(sf::Color::Black);
	pageTitle.setPosition(sf::Vector2f(325, 60));

	info = sf::Text("Title                        Author", fontAllura, 55);
	info.setFillColor(sf::Color::Black);
	info.setPosition(sf::Vector2f(160, 120));
}

void ProfileState::InitButtons()
{
	mainMenuButton = PushButton("Spre Meniu", { 200, 70 }, 30, sf::Color(255, 255, 255), sf::Color::Black);
	mainMenuButton.SetPosition(sf::Vector2f(100, 700));
	mainMenuButton.SetFont(fontAllura);
	mainMenuButton.SetOutline(2, sf::Color::Black);

	returnBookButton = PushButton("Returneaza Cartea", { 200, 70 }, 30, sf::Color(255, 255, 255), sf::Color::Black);
	returnBookButton.SetPosition(sf::Vector2f(350, 700));
	returnBookButton.SetFont(fontAllura);
	returnBookButton.SetOutline(2, sf::Color::Black);

	extendLoanButton = PushButton("Extinde Imprumut", { 200, 70 }, 30, sf::Color(255, 255, 255), sf::Color::Black);
	extendLoanButton.SetPosition(sf::Vector2f(600, 700));
	extendLoanButton.SetFont(fontAllura);
	extendLoanButton.SetOutline(2, sf::Color::Black);

	int y = 190;
	for (int i = 0; i < borrowedBooksButtons.size(); i++)
	{
		borrowedBooksButtons[i] = PushButton("", {700, 100}, 30, sf::Color(255, 255, 255), sf::Color::Black);
		borrowedBooksButtons[i].SetPosition(sf::Vector2f(100, y));
		borrowedBooksButtons[i].SetFont(fontAllura);
		borrowedBooksButtons[i].SetOutline(2, sf::Color::Black);
		y += 100;
	}
}

void ProfileState::InitVisuals()
{
	if (!borderTexture.loadFromFile("Textures/Border_Frame_2.png"))
	{
		std::cout << "Could not load from file." << std::endl;
	}
	border.setTexture(borderTexture);
	border.setScale(sf::Vector2f(1.2,1.2));
	border.setPosition(15, 10);

	ownedBooks = sf::RectangleShape(sf::Vector2f(700, 500));
	ownedBooks.setFillColor(sf::Color(205, 205, 205, 90));
	ownedBooks.setPosition(sf::Vector2f(100, 190));
	ownedBooks.setOutlineThickness(2);
	ownedBooks.setOutlineColor(sf::Color::Black);
}

void ProfileState::RenderVisuals(sf::Window& window)
{
	this->window->draw(border);
	this->window->draw(ownedBooks);

}

void ProfileState::RenderText(sf::Window& window)
{
	this->window->draw(pageTitle);
	this->window->draw(info);
}

void ProfileState::RenderButtons(sf::Window& window)
{
	mainMenuButton.DrawTo(*this->window);
	returnBookButton.DrawTo(*this->window);
	extendLoanButton.DrawTo(*this->window);
	for (auto box : borrowedBooksButtons)
	{
		box.DrawTo(*this->window);
	}
	RenderBooks(*this->window);
}

void ProfileState::RenderBooks(sf::Window& window)
{
	int limit;
	if (borrowedBooks.size() > 5)
	{
		limit = 5;
	}
	else
	{
		limit = borrowedBooks.size();
	}
	for (int i = 0; i < limit; i++)
	{
		sf::Text title(borrowedBooks[i].GetTitle(), fontArial, 20);
		title.setFillColor(sf::Color::Black);
		if (title.getGlobalBounds().width > 350)
		{
			title.setString(borrowedBooks[i].GetTitle().substr(0, 25) + "...");
		}
		title.setPosition(sf::Vector2f(120, 190 + (100 - title.getGlobalBounds().height) / 2 + 100 * i));

		sf::Text author(borrowedBooks[i].GetAuthor(), fontArial, 20);
		author.setFillColor(sf::Color::Black);
		if (author.getGlobalBounds().width > 350)
		{
			author.setString(borrowedBooks[i].GetTitle().substr(0, 25) + "...");
		}
		author.setPosition(sf::Vector2f(470, 190 + (100 - author.getGlobalBounds().height)/2 + 100 * i));

		this->window->draw(title);
		this->window->draw(author);
	}
}

bool ProfileState::IsMainMenuButtonPressed() {
	return mainMenuButtonPressed;
}

void ProfileState::SetUserId(const std::string& userId)
{
	this->userId = userId;
}

void ProfileState::GetBorrowedBooks()
{
	messageSent = SaveUserIdAndEventIdInJson(userId,"8").dump();
	connectSocket->Send(messageSent.c_str(), messageSent.size());

	connectSocket->Receive(receivedBuffer.data(), receivedBuffer.size(), received);
	std::string messageReceived(receivedBuffer.data(), received);

	json j = json::parse(messageReceived);
	for (int i = 0; i < j.size(); i++)
	{
		Book book(j, i);
		borrowedBooks.push_back(book);
	}
}
