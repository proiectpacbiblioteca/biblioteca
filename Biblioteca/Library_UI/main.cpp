#include <SFML/Graphics.hpp>
#include <iostream>
#include "Library_UI.h"

int main()
{
	TcpSocket connectSocket;
	connectSocket.Connect("localhost", 27015);
	Library_UI ui(&connectSocket);
	ui.Run();

	return 0;
}