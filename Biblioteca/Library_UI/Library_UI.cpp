#include "Library_UI.h"

#pragma comment(lib, "Ws2_32.lib")

Library_UI::Library_UI(TcpSocket *connectSocket)
{
	this->connectSocket = connectSocket;
	UpdateState(InterfaceState::LoginPage);
}

Library_UI::~Library_UI()
{
	delete window;
}

const bool Library_UI::IsWindowOpen() const	
{
	return this->window->isOpen();
}

void Library_UI::CenterText(sf::Text text)
{
	sf::FloatRect textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width / 2.0f,
		textRect.top + textRect.height / 2.0f);
	text.setPosition(window->getView().getCenter());
}

void Library_UI::UpdateState(InterfaceState interfaceState)
{
	switch (interfaceState)
	{
	case InterfaceState::LoginPage:
		state = std::make_shared<LoginState>(window);
		this->window = state->GetWindow();
		this->event = state->GetEvent();
		this->videoMode = state->GetVideoMode();
		state->SetSocket(this->connectSocket);
		break;
	case InterfaceState::MainPage:
		state = std::make_shared<MainMenuState>(window);
		this->window = state->GetWindow();
		this->event = state->GetEvent();
		this->videoMode = state->GetVideoMode();
		std::dynamic_pointer_cast<MainMenuState>(state)->SetUserId(this->userId);
		state->SetSocket(this->connectSocket);
		std::dynamic_pointer_cast<MainMenuState>(state)->UpdateBooks();
		break;
	case InterfaceState::ProfilePage:
		state = std::make_shared<ProfileState>(window);
		this->window = state->GetWindow();
		this->event = state->GetEvent();
		this->videoMode = state->GetVideoMode();
		std::dynamic_pointer_cast<ProfileState>(state)->SetUserId(this->userId);
		state->SetSocket(this->connectSocket);
		std::dynamic_pointer_cast<ProfileState>(state)->GetBorrowedBooks();
		break;
	default:
		state = std::make_shared<LoginState>(window);
		this->window = state->GetWindow();
		this->event = state->GetEvent();
		this->videoMode = state->GetVideoMode();
		state->SetSocket(this->connectSocket);
		break;
	}
}

void Library_UI::Initialize()
{
	state->InitVariables();
	state->InitWindow();
	state->InitText();
	state->InitButtons();
}

void Library_UI::Update()
{	
	state->PollEvents();
	if (state->HasEventProduced())
	{
		if (std::shared_ptr<LoginState> auxState = std::dynamic_pointer_cast<LoginState>(state))
		{
			if (auxState->IsLoginButtonPressed()&&auxState->IsUserNameValid())
			{
				this->window->close();
				userId = auxState->GetUserId();
				UpdateState(InterfaceState::MainPage);
			}
		}
		else if (std::shared_ptr<MainMenuState> auxState = std::dynamic_pointer_cast<MainMenuState>(state))
		{
			if (auxState->IsDisconnectButtonPressed())
			{
				this->window->close();
				UpdateState(InterfaceState::LoginPage);
			}
			else if (auxState->IsProfileButtonPressed())
			{
				this->window->close();
				UpdateState(InterfaceState::ProfilePage);
			}
		}
		else if (std::shared_ptr<ProfileState> auxState = std::dynamic_pointer_cast<ProfileState>(state))
		{
			if (auxState->IsMainMenuButtonPressed())
			{
				this->window->close();
				UpdateState(InterfaceState::MainPage);
			}
		}
		state->SetEventProduced(false);
	}
}

void Library_UI::Run()
{
	while (this->IsWindowOpen())
	{
		Update();
		Render();
	}
}

void Library_UI::Render()
{
	this->window->clear(sf::Color::White);
	
	state->RenderVisuals(*this->window);
	state->RenderText(*this->window);
	state->RenderButtons(*this->window);

	this->window->display();
}