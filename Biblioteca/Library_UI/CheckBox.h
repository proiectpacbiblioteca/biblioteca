#pragma once

#include <iostream>
#include <SFML/Graphics.hpp>

class CheckBox
{
private:
	//Variables
	bool isChecked;
	sf::Text label;
	sf::Font labelFont;
	sf::RectangleShape box;
	int labelSize;
	int boxSize;

	//Methods
	void drawTo(sf::RenderWindow& window)
	{
		window.draw(box);
		window.draw(label);
	}
	void onMouseReleased(float x, float y)
	{

	}
public:
	//Constructors
	CheckBox() {}
	CheckBox(sf::Text label, sf::Font font, int labelSize, int boxSize, sf::Vector2f boxPosition, bool isChecked = false)
	{
		this->isChecked = isChecked;
		this->label = label;
		this->labelFont = font;
		this->labelSize = labelSize;
		this->boxSize = boxSize;
		box.setPosition(boxPosition);
	}

	//Overloaded operators
	CheckBox& operator=(const CheckBox& checkbox)
	{
		this->label = checkbox.label;
		this->labelFont = checkbox.labelFont;
		this->labelSize = checkbox.labelSize;
		this->boxSize = checkbox.boxSize;
		box.setPosition(checkbox.box.getPosition());
		this->isChecked = checkbox.isChecked;
		return *this;
	}

	//Setters
	void setPosition(sf::Vector2f position)
	{
		box.setPosition(position);
	}
	void setLabelFont(sf::Font font)
	{
		this->labelFont = font;
	}
	void setLabel(sf::Text label)
	{
		this->label = label;
	}
	void setLabelSize(int size)
	{
		label.setCharacterSize(size);
	}
	void setBoxSize(sf::Vector2f size)
	{
		box.setSize(size);
	}

	//Getters
	sf::Vector2f getPosition()
	{
		return box.getPosition();
	}
	int getCharacterSize()
	{
		return label.getCharacterSize();
	}
	sf::Text getLabelText()
	{
		return label;
	}
};