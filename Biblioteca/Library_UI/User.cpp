#include "User.h"

User::User(const json& userInfo)
{
	this->username = userInfo["username"];
	this->userId = userInfo["userId"];
}

void User::SetUsername(const std::string& username)
{
	this->username = username;
}

void User::SetUserId(const std::string& userId)
{
	this->userId = userId;
}
