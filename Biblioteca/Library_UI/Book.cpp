#include "Book.h"

Book::Book(const json& bookInfo, int bookIndex)
{
	this->bookId = bookInfo["Book" + std::to_string(bookIndex)]["book_id"];
	this->isbn = bookInfo["Book" + std::to_string(bookIndex)]["isbn"];
	this->authors = bookInfo["Book" + std::to_string(bookIndex)]["authors"];
	this->originalPublicationYear = bookInfo["Book" + std::to_string(bookIndex)]["original_publication_year"];
	this->title = bookInfo["Book" + std::to_string(bookIndex)]["title"];
	this->languageCode = bookInfo["Book" + std::to_string(bookIndex)]["language_code"];
	this->average_rating = bookInfo["Book" + std::to_string(bookIndex)]["average_rating"];
	this->imageUrl = bookInfo["Book" + std::to_string(bookIndex)]["image_url"];
	this->smallImageUrl = bookInfo["Book" + std::to_string(bookIndex)]["small_image_url"];
	this->content = bookInfo["Book" + std::to_string(bookIndex)]["content"];
}

std::string Book::GetBookId()
{
	return bookId;
}

std::string Book::GetTitle()
{
	return title;
}

std::string Book::GetAuthor()
{
	return authors;
}

std::string Book::GetISBN()
{
	return isbn;
}

std::string Book::GetImageURL()
{
	return imageUrl;
}

std::string Book::GetRating()
{
	return average_rating;
}
