#pragma once
#include <SFML/Graphics.hpp>
#include<array>
#include <future>

#include <winsock2.h>	// contains most of the Winsock functions, structures, and definitions
#include <ws2tcpip.h>	// contains newer functions and structures used to retrieve IP addresses
#include "../NetworkingLibrary/TcpSocket.h"

class State
{
protected:
	sf::RenderWindow* window;
	sf::Event event;
	sf::VideoMode videoMode;
	TcpSocket* connectSocket;
	bool eventProduced = false;
	std::string received;
public:
	//Polling
	virtual void PollEvents()=0;

	//Initializers
	virtual void InitVariables() = 0;
	virtual void InitWindow() = 0;
	virtual void InitText() = 0;
	virtual void InitVisuals() = 0;
	virtual void InitButtons() = 0;

	//Renderers
	virtual void RenderVisuals(sf::Window& window) = 0;
	virtual void RenderText(sf::Window& window) = 0;
	virtual void RenderButtons(sf::Window& window) = 0;

	//Getters
	sf::RenderWindow* GetWindow() { return window; }
	sf::Event GetEvent() { return event; }
	sf::VideoMode GetVideoMode() { return videoMode; }
	TcpSocket* GetConnectSocket() { return connectSocket; }
	bool HasEventProduced() { return eventProduced; }
	
	//Setters
	void SetEventProduced(bool eventProduced) { this->eventProduced = eventProduced; }
	void SetSocket(TcpSocket* socket) { this->connectSocket = socket; }
};

