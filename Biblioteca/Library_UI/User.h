#pragma once
#include <string>
#include <vector>
#include "Book.h"
#include "../NetworkingLibrary/TcpSocket.h"

class User
{
private:
	std::string username;
	std::string userId;
public:
	User(const json& userInfo);
	User() = default;

	void SetUsername(const std::string& username);
	void SetUserId(const std::string& userId);
};