#pragma once
#ifndef LIBRARY_UI_H
#define LIBRARY_UI_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include <stack>
#include "Textbox.h"
#include "PushButton.h"
#include "LoginState.h"
#include "MainMenuState.h"
#include "ProfileState.h"
#include "State.h"


class Library_UI
{
protected:
	//General Variables
	sf::RenderWindow* window;
	sf::Event event;
	sf::VideoMode videoMode;
	TcpSocket *connectSocket;
	std::shared_ptr<State> state=nullptr;
	std::string userId;

	//Interface states
	enum class InterfaceState {
		LoginPage=0,
		MainPage,
		ProfilePage
	}interfaceState;
	void UpdateState(InterfaceState interfaceState);

	//Initializers
	void Initialize();

public:
	//Constructors & deconstructors
	Library_UI(TcpSocket* connectSocket);
	~Library_UI();

	//Methods
	const bool IsWindowOpen() const;
	void CenterText(sf::Text text);
	void Update();
	void Run();

	//Renderers
	void Render();
};

#endif