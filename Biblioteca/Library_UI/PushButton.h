#pragma once

#include <iostream>
#include <SFML/Graphics.hpp>

class PushButton {
public:
	//Constructors
	PushButton() {};

	PushButton(std::string label, sf::Vector2f size, int charSize, sf::Color bgColor, sf::Color textColor) {
		text.setString(label);
		text.setFillColor(textColor);
		text.setCharacterSize(charSize);
		button.setSize(size);
		button.setFillColor(bgColor);
	}

	//Setters
	void SetFont(sf::Font& font) {
		text.setFont(font);
		CenterText();
	}

	void SetBackColor(sf::Color color) {
		button.setFillColor(color);
	}

	void SetTextColor(sf::Color color) {
		text.setFillColor(color);
	}

	void SetPosition(sf::Vector2f pos) {
		button.setPosition(pos);
	}

	void SetOutline(int outlineThickness, sf::Color outlineColor)
	{
		button.setOutlineThickness(outlineThickness);
		button.setOutlineColor(outlineColor);
	}
	
	//Getters
	sf::Color GetBackColor()
	{
		return button.getFillColor();
	}
	sf::RectangleShape GetButton()
	{
		return button;
	}

	//Overloading operator
	PushButton& operator=(const PushButton& pushbutton) {
		text.setString(pushbutton.text.getString());
		text.setFillColor(pushbutton.text.getFillColor());
		text.setCharacterSize(pushbutton.text.getCharacterSize());

		button.setPosition(pushbutton.button.getPosition());
		button.setSize(pushbutton.button.getSize());
		button.setFillColor(pushbutton.button.getFillColor());
		return *this;
	}

	//Functionality
	void DrawTo(sf::RenderWindow& window) {
		window.draw(button);
		window.draw(text);
	}

	bool IsMouseOver(sf::RenderWindow& window) {
		float mouseX = sf::Mouse::getPosition(window).x;
		float mouseY = sf::Mouse::getPosition(window).y;

		float btnPosX = button.getPosition().x;
		float btnPosY = button.getPosition().y;

		float btnxPosWidth = button.getPosition().x + button.getLocalBounds().width;
		float btnyPosHeight = button.getPosition().y + button.getLocalBounds().height;

		if (mouseX < btnxPosWidth && mouseX > btnPosX && mouseY < btnyPosHeight && mouseY > btnPosY) {
			return true;
		}
		return false;
	}

	void CenterText()
	{
		sf::FloatRect buttonBounds = button.getGlobalBounds();
		sf::FloatRect textBounds = text.getGlobalBounds();
		text.setPosition(
			button.getPosition().x + (buttonBounds.width / 2) - textBounds.width / 2,
			button.getPosition().y + (buttonBounds.height / 2) - textBounds.height
		);
	}
private:
	//Variables
	std::string label;
	sf::RectangleShape button;
	sf::Text text;
};
