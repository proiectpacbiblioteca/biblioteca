#pragma once

#include <iostream>
#include <SFML/Graphics.hpp>
#include <sstream>

#define DELETE_KEY 8
#define ENTER_KEY 13
#define ESCAPE_KEY 27

class Textbox
{
public:
	//constructors
	Textbox() 
	{
		textbox.setCharacterSize(14);
		SetPosition(sf::Vector2f(100, 100));
		textbox.setFillColor(sf::Color::Black);
		SetStyle(sf::Text::Regular);
		isSelected = false;
		this->SetLimit(true, 50);
		this->SetBoxColor(sf::Color(220, 220, 220));
		this->overLimit = 0;
	}
	Textbox(int size, sf::Vector2f position = sf::Vector2f(0,0),int limit=50, sf::Color color = sf::Color::Black, bool selected = false)
	{
		textbox.setCharacterSize(size);
		SetPosition(position);
		textbox.setFillColor(color);
		SetStyle(sf::Text::Regular);
		isSelected = selected;
		if (selected)
		{
			textbox.setString("_");
		}
		else
		{
			textbox.setString("");
		}
		this->SetLimit(true, limit);
		this->SetBoxColor(sf::Color(200,200,200));
	}

	//Overloaded operators
	Textbox& operator=(const Textbox& var)
	{
		this->textbox.setCharacterSize(var.textbox.getCharacterSize());
		this->SetPosition(var.textbox.getPosition());
		this->textbox.setFillColor(var.textbox.getFillColor());
		this->isSelected = var.isSelected;
		if (isSelected)
		{
			this->textbox.setString("_");
		}
		else
		{
			this->textbox.setString("");
		}
		this->SetLimit(true, var.limit);
		this->SetStyle(var.textbox.getStyle());
		return *this;
	}

	//Setters
	void SetFont(sf::Font& font)
	{
		textbox.setFont(font);
	}

	void SetPosition(sf::Vector2f position)
	{
		textbox.setPosition(position);
		box.setPosition(sf::Vector2f(position.x - 10, position.y));
	}

	void SetLimit(bool button, int limit)
	{
		hasLimit = button;
		this->limit = limit;
	}

	void SetBoxColor(sf::Color color)
	{
		box.setFillColor(color);
	}

	void SetBoxSize(float height, float width)
	{
		box.setSize(sf::Vector2f(height, width));
	}

	void SetStyle(sf::Uint32 style)
	{
		textbox.setStyle(style);
	}

	void SetSelected(bool selected)
	{
		isSelected = selected;
		if (selected)
		{
			textbox.setString(text.str() + "_");
		}
		else
		{
			textbox.setString(text.str());
		}
	}

	void SetOutline(int outlineThickness, sf::Color outlineColor)
	{
		box.setOutlineThickness(outlineThickness);
		box.setOutlineColor(outlineColor);
	}

	void SetText(std::string text)
	{
		textbox.setString(text);
	}

	bool IsMouseOver(sf::RenderWindow& window) {
		float mouseX = sf::Mouse::getPosition(window).x;
		float mouseY = sf::Mouse::getPosition(window).y;

		float btnPosX = box.getPosition().x;
		float btnPosY = box.getPosition().y;

		float btnxPosWidth = box.getPosition().x + box.getLocalBounds().width;
		float btnyPosHeight = box.getPosition().y + box.getLocalBounds().height;

		if (mouseX < btnxPosWidth && mouseX > btnPosX && mouseY < btnyPosHeight && mouseY > btnPosY) {
			return true;
		}
		return false;
	}

	//Getters
	std::string GetText()
	{
		return text.str();
	}
	unsigned int GetCharacterSize()
	{
		return textbox.getCharacterSize();
	}
	unsigned int GetLimit()
	{
		return limit;
	}

	//Methods
	void Empty()
	{
		isSelected = true;
		while (textbox.getString() != "\0")
		{
			DeleteLastChar();
		}
		isSelected = false;
	}

	void DrawTo(sf::RenderWindow& window)
	{
		window.draw(box);
		window.draw(textbox);
	}

	void TypedOn(sf::Event event)
	{
		if (isSelected)
		{
			int typedChar = event.text.unicode;
			if (typedChar < 128)
			{
				if (hasLimit)
				{
					if (text.str().length() <= limit)
					{
						InputLogic(typedChar);
					}
					else if (text.str().length() > limit && typedChar == DELETE_KEY)
					{
						DeleteLastChar();
					}
				}
				else
				{
					InputLogic(typedChar);
				}
			}
		}
	}
private:
	//Variables
	sf::RectangleShape box;
	sf::Text textbox;
	std::ostringstream text;
	bool isSelected = false;
	bool hasLimit = false;
	int limit;
	int textLength;
	int overLimit;

	//Basic functionality
	void InputLogic(int typedChar)
	{
		if (typedChar != DELETE_KEY && typedChar != ENTER_KEY && typedChar != ESCAPE_KEY)
		{
			text << static_cast<char>(typedChar);
			textLength += textbox.getFont()->getGlyph(typedChar, this->GetCharacterSize(), false, 1).bounds.width;
			
			if (textLength >= box.getSize().x)
			{
				overLimit++;
				textbox.setString(text.str().substr(overLimit)+"_");
			}
			else
			{
				textbox.setString(text.str() + "_");
			}
		}
		else if (typedChar == DELETE_KEY)
		{
			if (text.str().length() > 0)
			{
				textLength -= textbox.getFont()->getGlyph(text.str()[text.str().size()-1], this->GetCharacterSize(), false, 1).bounds.width;
				DeleteLastChar();

				if (overLimit>0)
				{
					overLimit--;
				}
				textbox.setString(text.str().substr(overLimit) + "_");
			}
		}
	}

	void DeleteLastChar()
	{
		std::string t = text.str().substr(0, text.str().size() - 1);
		text.str("");
		text << t;
		textbox.setString(text.str());
	}
};