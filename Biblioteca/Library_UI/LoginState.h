#pragma once
#include "State.h"
#include "Textbox.h"
#include "PushButton.h"

class LoginState :
    public State
{
protected:
	//General Variables
	bool loginButtonPressed = false;
	bool signUpButtonPressed = false;
	bool nameValid = false;
	sf::Clock clock;

	//Text Variables
	Textbox loginBox;
	sf::Text welcomeMessage1;
	sf::Text welcomeMessage2;
	sf::Text loginIntruction;
	sf::Text connectionStatus;

	//Font Variables
	sf::Font fontArial;
	sf::Font fontAllura;

	//Button Variables
	PushButton buttonSignUp, buttonLogin;

	//Visual Variables
	sf::Texture border;
	sf::Sprite borderSprite;

	//Variables needed for client-server
	std::string messageSent;
	std::array<char, 8192> receivedBuffer;
	int received;

public:
	//Constructor
	LoginState(sf::RenderWindow* window);

	//Polling
	virtual void PollEvents();

	//Initializers
	virtual void InitVariables() override;
	virtual void InitWindow() override;
	virtual void InitText() override;
	virtual void InitButtons() override;
	virtual void InitVisuals() override;

	//Renderers
	virtual void RenderVisuals(sf::Window& window) override;
	virtual void RenderText(sf::Window& window) override;
	virtual void RenderButtons(sf::Window& window) override;

	//Methods
	bool IsUserNameValid();
	bool IsLoginButtonPressed();
	
	//Getters
	std::string GetUserId();
};

