#include "LoginState.h"
#include <future>

json SaveUsernameAndEventInJson(const std::string& username, const std::string& eventId)
{
	json jsonMessage;
	jsonMessage["data1"] = username;
	jsonMessage["event"] = eventId;
	return jsonMessage;
}

LoginState::LoginState(sf::RenderWindow* window)
{
	this->InitVariables();
	this->InitWindow();
	this->InitVisuals();
	this->InitText();
	this->InitButtons();
}

void LoginState::PollEvents()
{
	while (this->window->pollEvent(this->event))
	{
		switch (this->event.type)
		{
		case sf::Event::Closed:
			this->window->close();
			break;
		case sf::Event::TextEntered:
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
			{
				clock.restart();
				loginButtonPressed = true;

				messageSent = SaveUsernameAndEventInJson(loginBox.GetText(), "2").dump();

				connectSocket->Send(messageSent.c_str(), messageSent.size());

				connectSocket->Receive(receivedBuffer.data(), receivedBuffer.size(), received);
				std::string messageReceived(receivedBuffer.data(), received);

				if (messageReceived == "accepted")
				{
					nameValid = true;
				}
			}
			else
			{
				loginBox.TypedOn(event);
			}
			break;
		case sf::Event::MouseMoved:
			if (buttonSignUp.IsMouseOver(*this->window))
			{
				buttonSignUp.SetBackColor(sf::Color(180, 180, 180));
			}
			else
			{
				buttonSignUp.SetBackColor(sf::Color(200, 200, 200));
			}
			if (buttonLogin.IsMouseOver(*this->window))
			{
				buttonLogin.SetBackColor(sf::Color(180, 180, 180));
			}
			else
			{	
				buttonLogin.SetBackColor(sf::Color(200, 200, 200));
			}
			break;
		case sf::Event::MouseButtonPressed:
			if (event.mouseButton.button == sf::Mouse::Left)
			{
				if (buttonSignUp.IsMouseOver(*this->window) && loginBox.GetText() != "")
				{
					clock.restart();
					signUpButtonPressed = true;

					buttonSignUp.SetBackColor(sf::Color(100, 100, 100));

					messageSent = SaveUsernameAndEventInJson(loginBox.GetText(), "1").dump();

					connectSocket->Send(messageSent.c_str(), messageSent.size());

					connectSocket->Receive(receivedBuffer.data(), receivedBuffer.size(), received);
					std::string messageReceived(receivedBuffer.data(), received);

					if (messageReceived == "accepted")
					{
						nameValid = true;
					}
				}
				if (buttonLogin.IsMouseOver(*this->window) && loginBox.GetText() != "")
				{
					clock.restart();
					loginButtonPressed = true;
					buttonLogin.SetBackColor(sf::Color(100, 100, 100));

					messageSent = SaveUsernameAndEventInJson(loginBox.GetText(), "2").dump();

					connectSocket->Send(messageSent.c_str(), messageSent.size());

					connectSocket->Receive(receivedBuffer.data(), receivedBuffer.size(), received);
					std::string messageReceived(receivedBuffer.data(), received);

					if (messageReceived == "accepted")
					{
						nameValid = true;
					}
				}
				if(loginBox.IsMouseOver(*this->window))
				{
					loginBox.SetSelected(true);
				}
				else
				{
					loginBox.SetSelected(false);
				}
			}
			break;
		}
	}
}

void LoginState::InitVariables()
{
	this->window = nullptr;
}

void LoginState::InitWindow()
{
	this->videoMode.width = 600;
	this->videoMode.height = 800;

	this->window = new sf::RenderWindow(this->videoMode, "Autentificare - Biblioteca", sf::Style::Close);
	this->window->setFramerateLimit(60);
	this->window->setPosition(sf::Vector2i(660,70));
}

void LoginState::InitText()
{
	if (!fontArial.loadFromFile("Fonts/arial.ttf"))
	{
		std::cout << "Could not load font from file." << std::endl;
		this->window->close();
	}
	if (!fontAllura.loadFromFile("Fonts/Allura-Regular.ttf"))
	{
		std::cout << "Could not load font from file." << std::endl;
		this->window->close();
	}
	loginBox = Textbox(20, sf::Vector2f(90, 480), 30, sf::Color::Black);
	loginBox.SetFont(fontArial);
	loginBox.SetBoxSize(loginBox.GetLimit() * fontArial.getGlyph(65, loginBox.GetCharacterSize(), false).bounds.width, fontArial.getLineSpacing(loginBox.GetCharacterSize()) * 1.2);

	welcomeMessage1 = sf::Text("Bun venit", fontAllura, 60);
	welcomeMessage1.setFillColor(sf::Color::Black);
	welcomeMessage1.setPosition(sf::Vector2f(190, 180));
	welcomeMessage2 = sf::Text("la Biblioteca Digitala!", fontAllura, 60);
	welcomeMessage2.setFillColor(sf::Color::Black);
	welcomeMessage2.setPosition(sf::Vector2f(60, 230));

	loginIntruction = sf::Text("Introduceti numele de utilizator:", fontAllura, 40);
	loginIntruction.setFillColor(sf::Color::Black);
	loginIntruction.setPosition(sf::Vector2f(80, 430));

	connectionStatus = sf::Text("Se conecteaza...", fontAllura, 40);
	connectionStatus.setFillColor(sf::Color::Black);
	connectionStatus.setPosition(sf::Vector2f(80, 520));
}

void LoginState::InitButtons()
{
	buttonSignUp = PushButton("Creare Cont Nou", { 150, 50 }, 24, sf::Color(200, 200, 200), sf::Color::Black);
	buttonSignUp.SetPosition({ 100, 600 });
	buttonSignUp.SetFont(fontAllura);

	buttonLogin = PushButton("Autentificare", { 150, 50 }, 24, sf::Color(200, 200, 200), sf::Color::Black);
	buttonLogin.SetPosition({ 350, 600 });
	buttonLogin.SetFont(fontAllura);
}

void LoginState::InitVisuals()
{
	if (!border.loadFromFile("Textures/Elegant_Border.png"))
	{
		return;
	}
	borderSprite.setTexture(border);
	borderSprite.setTextureRect(sf::IntRect(-5, 0, border.getSize().x, border.getSize().y));
}

void LoginState::RenderVisuals(sf::Window& window)
{
	this->window->draw(borderSprite);
}

void LoginState::RenderText(sf::Window& window)
{
	loginBox.DrawTo(*this->window);
	this->window->draw(welcomeMessage1);
	this->window->draw(welcomeMessage2);
	this->window->draw(loginIntruction);
	if (loginButtonPressed)
	{
		if (clock.getElapsedTime() > sf::seconds(2) && clock.getElapsedTime() < sf::seconds(4))
		{
			if (nameValid)
			{
				connectionStatus.setString("Numele este valid");
				eventProduced = true;
			}
			else
			{
				connectionStatus.setString("Numele nu este valid");
			}
			this->window->draw(connectionStatus);
		}
		else if(clock.getElapsedTime() > sf::seconds(4))
		{
			loginButtonPressed = false;
			connectionStatus.setString("Se conecteaza...");
		}
		else
		{
			this->window->draw(connectionStatus);
		}
	}
	else if (signUpButtonPressed)
	{
		if (clock.getElapsedTime() > sf::seconds(2) && clock.getElapsedTime() < sf::seconds(4))
		{
			if (nameValid)
			{
				connectionStatus.setString("Numele este valid");
			}
			else
			{
				connectionStatus.setString("Numele introdus exista deja");
			}
			this->window->draw(connectionStatus);
		}
		else if (clock.getElapsedTime() > sf::seconds(4))
		{
			connectionStatus.setString("Se conecteaza...");
		}
		else
		{
			this->window->draw(connectionStatus);
		}
	}
}

void LoginState::RenderButtons(sf::Window& window) 
{
	this->buttonSignUp.DrawTo(*this->window);
	this->buttonLogin.DrawTo(*this->window);
}

bool LoginState::IsUserNameValid()
{
	return nameValid;
}

bool LoginState::IsLoginButtonPressed()
{
	return loginButtonPressed;
}

std::string LoginState::GetUserId()
{
	if (nameValid)
	{
		messageSent = SaveUsernameAndEventInJson(loginBox.GetText(), "3").dump();
		connectSocket->Send(messageSent.c_str(), messageSent.size());

		connectSocket->Receive(receivedBuffer.data(), receivedBuffer.size(), received);
		std::string messageReceived(receivedBuffer.data(), received);
		return messageReceived;
	}
	else
	{
		return "\0";
	}
}
