#pragma once
#include "State.h"
#include "PushButton.h"
#include "Textbox.h"
#include "Book.h"

class ProfileState :
	public State
{
private:
	//Text Variables
	sf::Font fontAllura;
	sf::Font fontArial;
	sf::Text pageTitle;
	sf::Text info;

	//Visual Variables
	sf::RectangleShape ownedBooks;

	//Texture Variables
	sf::Texture borderTexture;
	sf::Sprite border;

	//Button Variables
	PushButton mainMenuButton;
	PushButton returnBookButton;
	PushButton extendLoanButton;
	std::vector<PushButton> borrowedBooksButtons;

	//General Variables
	bool mainMenuButtonPressed;
	bool returnButtonPressed;
	std::vector<Book> borrowedBooks;
	std::string userId;

	//Variables needed for client-servers
	std::string messageSent;
	std::array<char, 8192> receivedBuffer;
	int received;
	int returnedBookIndex = -1;


public:
	//Constructor
	ProfileState(sf::RenderWindow* window);

	//Polling
	virtual void PollEvents();

	//Initializers
	virtual void InitVariables() override;
	virtual void InitWindow() override;
	virtual void InitText() override;
	virtual void InitButtons() override;
	virtual void InitVisuals() override;

	//Renderers
	virtual void RenderVisuals(sf::Window& window) override;
	virtual void RenderText(sf::Window& window) override;
	virtual void RenderButtons(sf::Window& window) override;
	void RenderBooks(sf::Window& window);

	//Other Methods
	bool IsMainMenuButtonPressed();
	void SetUserId(const std::string& userId);
	void GetBorrowedBooks();
};
