#include "MainMenuState.h"

json SaveEventIdInJson(const std::string& eventId)
{
	json jsonMessage;
	jsonMessage["event"] = eventId;
	return jsonMessage;
}

json SaveUserIdEventIdAndBookIdInJson(const std::string& userId, const std::string& bookId, const std::string& eventId)
{
	json jsonMessage;
	jsonMessage["event"] = eventId;
	jsonMessage["data1"] = userId;
	jsonMessage["data2"] = bookId;
	return jsonMessage;
}

MainMenuState::MainMenuState(sf::RenderWindow* window)
{
	this->InitVariables();
	this->InitWindow();
	this->InitVisuals();
	this->InitText();
	this->InitButtons();
}

void MainMenuState::PollEvents()
{
	while (this->window->pollEvent(this->event))
	{
		switch (this->event.type)
		{
		case sf::Event::Closed:
			this->window->close();
			break;
		case sf::Event::TextEntered:
			searchBar.TypedOn(event);
			if (searchBar.GetText() != "")
			{
				searchBarEmpty = false;
			}
			else
			{
				searchBarEmpty = true;
			}
			break;
		case sf::Event::MouseMoved:
			if (searchButtonAuthor.IsMouseOver(*this->window))
			{
				searchButtonAuthor.SetBackColor(sf::Color(230, 230, 230));
			}
			else
			{
				searchButtonAuthor.SetBackColor(sf::Color::White);
			}
			if (searchButtonName.IsMouseOver(*this->window))
			{
				searchButtonName.SetBackColor(sf::Color(230, 230, 230));
			}
			else
			{
				searchButtonName.SetBackColor(sf::Color::White);
			}
			if (searchButtonISBN.IsMouseOver(*this->window))
			{
				searchButtonISBN.SetBackColor(sf::Color(230, 230, 230));
			}
			else
			{
				searchButtonISBN.SetBackColor(sf::Color::White);
			}

			if (disconnectButton.IsMouseOver(*this->window))
			{
				disconnectButton.SetBackColor(sf::Color(230, 230, 230));
			}
			else
			{
				disconnectButton.SetBackColor(sf::Color::White);
			}
			if (profileButton.IsMouseOver(*this->window))
			{
				profileButton.SetBackColor(sf::Color(230, 230, 230));
			}
			else
			{
				profileButton.SetBackColor(sf::Color::White);
			}
			if (borrowButton.IsMouseOver(*this->window))
			{
				borrowButton.SetBackColor(sf::Color(230, 230, 230));
			}
			else
			{
				borrowButton.SetBackColor(sf::Color::White);
			}
			break;
		case sf::Event::MouseButtonPressed:
			if (event.mouseButton.button == sf::Mouse::Left)
			{
				if (leftButton.IsMouseOver(*this->window))
				{
					if (pageNumber>1)
					{
						pageNumber --;
						searchBar.Empty();
					}
					pageNumberText.setString(std::to_string(pageNumber));
					pageNumberText.setPosition(sf::Vector2f((window->getSize().x - pageNumberText.getGlobalBounds().width) / 2, 880));
				}
				else if (rightButton.IsMouseOver(*this->window))
				{
					pageNumber ++;
					searchBar.Empty();
					pageNumberText.setString(std::to_string(pageNumber));
					pageNumberText.setPosition(sf::Vector2f((window->getSize().x - pageNumberText.getGlobalBounds().width) / 2, 880));
					UpdateBooks();
				}

				if (searchBar.IsMouseOver(*this->window))
				{
					searchBar.SetSelected(true);
				}
				else
				{
					searchBar.SetSelected(false);
				}

				if (searchButtonAuthor.IsMouseOver(*this->window))
				{
					searchButtonAuthor.SetBackColor(sf::Color(210, 210, 210));
				}
				if(searchButtonName.IsMouseOver(*this->window))
				{
					searchButtonName.SetBackColor(sf::Color(210, 210, 210));
				}
				if (searchButtonISBN.IsMouseOver(*this->window))
				{
					searchButtonISBN.SetBackColor(sf::Color(210, 210, 210));
				}

				if (disconnectButton.IsMouseOver(*this->window))
				{
					disconnectButton.SetBackColor(sf::Color(210, 210, 210));
					disconnectButtonPressed = true;
					eventProduced = true;
				}
				else
				{
					disconnectButtonPressed = false;
				}
				if (profileButton.IsMouseOver(*this->window))
				{
					profileButton.IsMouseOver(*this->window);
					profileButtonPressed = true;
					eventProduced = true;
				}
				else
				{
					profileButtonPressed = false;
				}

				if (borrowButton.IsMouseOver(*this->window))
				{
					borrowButton.SetBackColor(sf::Color(210, 210, 210));
					if (selectedBookIndex >= 0)
					{
						borrowButtonPressed = true;
						clock.restart();

						messageSent = SaveUserIdEventIdAndBookIdInJson(userId, books[selectedBookIndex].GetBookId(), "9").dump();
						connectSocket->Send(messageSent.c_str(), messageSent.size());

						connectSocket->Receive(receivedBuffer.data(), receivedBuffer.size(), received);
						borrowMessage = std::string (receivedBuffer.data(), received);
					}
				}
				else 
				{
					borrowButtonPressed = false;
				}

				for (int i = 0; i < 6; i++)
				{
					if (buttonVector[i].IsMouseOver(*this->window))
					{
						buttonVector[i].SetBackColor(sf::Color(210, 210, 210));
						selectedBookIndex = i + 6 * (pageNumber-1);
					}
					else
					{
						buttonVector[i].SetBackColor(sf::Color::White);
					}
				}
			}
			break;
		}
	}
}

void MainMenuState::InitVariables()
{
	this->window = nullptr;
	disconnectButtonPressed = false;
	profileButtonPressed = false;
	borrowButtonPressed = false;
	searchBarEmpty = true;
	selectedBookIndex = -1;
	pageNumber = 1;
	starRating.resize(5);
}

void MainMenuState::InitWindow()
{
	this->videoMode.width = 1400;
	this->videoMode.height = 1000;

	this->window = new sf::RenderWindow(this->videoMode, "Pagina Principala", sf::Style::Close);
	this->window->setFramerateLimit(60);
	this->window->setPosition(sf::Vector2i(260, 0));
}

void MainMenuState::InitText()
{
	if (!fontAllura.loadFromFile("Fonts/Allura-Regular.ttf"))
	{
		std::cout << "Could not load font from file." << std::endl;
		this->window->close();
	}
	if (!fontArial.loadFromFile("Fonts/Arial.ttf"))
	{
		std::cout << "Could not load font from file." << std::endl;
		this->window->close();
	}
	searchBar = Textbox(30, sf::Vector2f(110, 130), 60, sf::Color::Black);
	searchBar.SetFont(fontArial);
	searchBar.SetBoxSize(1200 ,fontArial.getLineSpacing(searchBar.GetCharacterSize()) * 1.2);
	searchBar.SetOutline(2, sf::Color::Black);
	searchBar.SetText("Introduceti termenul de cautare...");

	pageNumberText = sf::Text(std::to_string(pageNumber), fontAllura, 40);
	pageNumberText.setFillColor(sf::Color::Black);
	pageNumberText.setPosition(sf::Vector2f((window->getSize().x - pageNumberText.getGlobalBounds().width) / 2, 880));

	popUpText = sf::Text("", fontAllura, 40);
	popUpText.setFillColor(sf::Color::Black);
}

void MainMenuState::InitButtons()
{
	if (!arrowTexture.loadFromFile("Textures/Elegant_Arrow.png"))
	{
		std::cout << "Could not load from file." << std::endl;
	}

	leftButton = PushButton("", { 250,93 }, 20, sf::Color(255, 255, 255), sf::Color::Black);
	leftButton.SetPosition({ 100,900 });

	arrowLeft.setTexture(arrowTexture);
	arrowLeft.setScale(sf::Vector2f(-1, 1));
	arrowLeft.setPosition(sf::Vector2f(350, 900));

	rightButton = PushButton("", { 250,93 }, 20, sf::Color(255, 255, 255), sf::Color::Black);
	rightButton.SetPosition({ 1050,900 });

	arrowRight.setTexture(arrowTexture);
	arrowRight.setPosition(sf::Vector2f(1050, 900));

	searchButtonName = PushButton("Cautare dupa denumire", { 398,50 }, 30, sf::Color(255, 255, 255), sf::Color::Black);
	searchButtonName.SetPosition({ 100, 172.5 });
	searchButtonName.SetFont(fontAllura);
	searchButtonName.SetOutline(2, sf::Color::Black);

	searchButtonAuthor = PushButton("Cautare dupa autor", { 400,50 }, 30, sf::Color(255, 255, 255), sf::Color::Black);
	searchButtonAuthor.SetPosition({ 498,172.5 });
	searchButtonAuthor.SetFont(fontAllura);
	searchButtonAuthor.SetOutline(2, sf::Color::Black);

	searchButtonISBN = PushButton("Cautare dupa ISBN", { 400,50 }, 30, sf::Color(255, 255, 255), sf::Color::Black);
	searchButtonISBN.SetPosition({ 900, 172.5 });
	searchButtonISBN.SetFont(fontAllura);
	searchButtonISBN.SetOutline(2, sf::Color::Black);

	disconnectButton = PushButton("Deconectare", { 450,50 }, 40, sf::Color(255, 255, 255), sf::Color::Black);
	disconnectButton.SetPosition({ 250, 0 });
	disconnectButton.SetFont(fontAllura);
	disconnectButton.SetOutline(2, sf::Color::Black);

	profileButton = PushButton("Profil", { 450,50 }, 40, sf::Color(255, 255, 255), sf::Color::Black);
	profileButton.SetPosition({ 700, 0 });
	profileButton.SetFont(fontAllura);
	profileButton.SetOutline(2, sf::Color::Black);

	borrowButton = PushButton("Imprumuta", { 450,50 }, 40, sf::Color(255, 255, 255), sf::Color::Black);
	borrowButton.SetPosition({ 475, 52 });
	borrowButton.SetFont(fontAllura);
	borrowButton.SetOutline(2, sf::Color::Black);
}

void MainMenuState::InitVisuals()
{
	if (!cornerTexture.loadFromFile("Textures/Elegant_Corner2.png"))
	{
		std::cout << "Could not load from file." << std::endl;
	}
	if (!detailTexture.loadFromFile("Textures/Elegant_Detail1.png"))
	{
		std::cout << "Could not load from file." << std::endl;
	}
	if (!fullStar.loadFromFile("Textures/full_star.png"))
	{
		std::cout << "Could not load from file." << std::endl;
	}
	if (!halfStar.loadFromFile("Textures/half_star.png"))
	{
		std::cout << "Could not load from file." << std::endl;
	}
	if (!emptyStar.loadFromFile("Textures/empty_star.png"))
	{
		std::cout << "Could not load from file." << std::endl;
	}

	corner1.setTexture(cornerTexture);
	corner1.setPosition(1190, 10);

	corner2.setTexture(cornerTexture);
	corner2.setScale(sf::Vector2f(-1, 1));
	corner2.setPosition(210, 10);

	detail.setTexture(detailTexture);
	detail.setPosition(405,900);

	booksBox = sf::RectangleShape(sf::Vector2f(1200, 650));
	booksBox.setFillColor(sf::Color(205, 205, 205, 90));
	booksBox.setPosition(sf::Vector2f(100, 225));
	booksBox.setOutlineThickness(2);
	booksBox.setOutlineColor(sf::Color::Black);

	popUp = sf::RectangleShape(sf::Vector2f(400, 80));
	popUp.setFillColor(sf::Color(230, 230, 230));
	popUp.setPosition(sf::Vector2f((this->window->getSize().x - 400) / 2 - 1, (this->window->getSize().y+10) / 2));
	popUp.setOutlineThickness(5);
	popUp.setOutlineColor(sf::Color::Black);

	int x = 100;
	int z = 100;
	buttonVector.resize(6);
	for (int i = 0; i < 6; i++)
	{
		if (i < 3)
		{
			buttonVector[i] = PushButton("",sf::Vector2f(400,325),30,sf::Color::White,sf::Color::Black);
			buttonVector[i].SetPosition(sf::Vector2f(x, 225));
			buttonVector[i].SetOutline(2,sf::Color::Black);
			x += 400;
		}
		if (i >= 3)
		{
			buttonVector[i] = PushButton("", sf::Vector2f(400, 325), 30, sf::Color::White, sf::Color::Black);
			buttonVector[i].SetPosition(sf::Vector2f(z, 550));
			buttonVector[i].SetOutline(2, sf::Color::Black);
			z += 400;
		}
	}
}

void MainMenuState::RenderVisuals(sf::Window& window)
{
	this->window->draw(booksBox);
	this->window->draw(corner1);
	this->window->draw(corner2);
	this->window->draw(detail);
	for (auto rect : buttonVector)
	{
		rect.DrawTo(*this->window);
	}
	RenderBooks(*this->window);
}

void MainMenuState::RenderText(sf::Window& window)
{
	this->window->draw(pageNumberText);
	searchBar.DrawTo(*this->window);
	if (borrowButtonPressed)
	{
		if (clock.getElapsedTime() <= sf::seconds(3))
		{
			if (borrowMessage == "accepted")
			{
				popUpText.setString("Succes!");
				popUpText.setPosition(sf::Vector2f(this->window->getSize().x / 2 - 50, this->window->getSize().y / 2 + 10));
				this->window->draw(popUp);
				this->window->draw(popUpText);
				selectedBookIndex = -1;
			}
			else
			{
				popUpText.setString("Nu se poate imprumuta!");
				popUpText.setPosition(sf::Vector2f(sf::Vector2f(this->window->getSize().x / 2 - 160, this->window->getSize().y / 2 + 10)));
				this->window->draw(popUp);
				this->window->draw(popUpText);
			}
		}
	}
}

void MainMenuState::RenderButtons(sf::Window& window)
{
	leftButton.DrawTo(*this->window);
	rightButton.DrawTo(*this->window);
	this->window->draw(arrowLeft);
	this->window->draw(arrowRight);
	searchButtonAuthor.DrawTo(*this->window);
	searchButtonName.DrawTo(*this->window);
	searchButtonISBN.DrawTo(*this->window);
	borrowButton.DrawTo(*this->window);
	disconnectButton.DrawTo(*this->window);
	profileButton.DrawTo(*this->window);
}

void MainMenuState::RenderBooks(sf::Window& window)
{
	int position = 210;
	int indexBook = (pageNumber - 1) * 6;
	int j = 0;
	for (int i = indexBook; i < indexBook + 6; i++)
	{
		DownloadHttp(books[i].GetImageURL(),j);
		sf::Image cover;
		if (!cover.loadFromFile("Images/book"+std::to_string(j)+".jpg"))
		{
			std::cout << "Could not load from file." << std::endl;
		}
		sf::Texture coverTexture;
		coverTexture.loadFromImage(cover);
		sf::Sprite coverSprite;
		coverSprite.setTexture(coverTexture);
		coverSprite.setPosition(sf::Vector2f(100 + 400 * (j % 3) + 150, 235 + 325 * (int)(j / 3)));

		sf::Text title(books[i].GetTitle(), fontArial, 20);
		title.setFillColor(sf::Color::Black);
		if (title.getGlobalBounds().width > 400)
		{
			title.setString(books[i].GetTitle().substr(0, 30) + "...");
		}
		title.setPosition(sf::Vector2f(buttonVector[j].GetButton().getGlobalBounds().width / 2 - title.getGlobalBounds().width / 2 + 400 * (j % 3) + 100, 400 + 325 * (int)(j / 3)));

		sf::Text author(books[i].GetAuthor(), fontArial, 20);
		author.setFillColor(sf::Color::Black);
		if (author.getGlobalBounds().width > 400)
		{
			author.setString(books[i].GetTitle().substr(0, 30) + "...");
		}

		author.setPosition(sf::Vector2f(buttonVector[j].GetButton().getGlobalBounds().width / 2 - author.getGlobalBounds().width / 2 + 400 * (j % 3) + 100, 425 + 325 * (int)(j / 3)));

		sf::Text isbn(books[i].GetISBN(), fontArial, 20);
		isbn.setFillColor(sf::Color::Black);
		isbn.setPosition(sf::Vector2f(buttonVector[j].GetButton().getGlobalBounds().width / 2 - isbn.getGlobalBounds().width / 2 + 400 * (j % 3) + 100, 450 + 325 * (int)(j / 3)));


		float rating = std::stof(books[i].GetRating());
		std::vector<float> ratingNum = ConvertRating(rating);
		for (int k = 0; k < 5; k++)
		{
			if (ratingNum[k] == 1)
				starRating[k].setTexture(fullStar);
			if (ratingNum[k] == 0.5)
				starRating[k].setTexture(halfStar);
			if (ratingNum[k] == 0)
				starRating[k].setTexture(emptyStar);
		}


		for (int k = 0; k < 5; k++)
		{
			starRating[k].setPosition(position, 500 + 325 * (int)(j / 3));
			this->window->draw(starRating[k]);
			position += 40;
		}
		if (j == 2)
		{
			position = 210;
		}
		else
		{
			position += 200;
		}

		this->window->draw(coverSprite);
		this->window->draw(title);
		this->window->draw(author);
		this->window->draw(isbn);
		j++;
	}
}

bool MainMenuState::IsDisconnectButtonPressed()
{
	return disconnectButtonPressed;
}

bool MainMenuState::IsProfileButtonPressed()
{
	return profileButtonPressed;
}

void MainMenuState::DownloadHttp(std::string imageURL, int index)
{
	//string splitting
	unsigned skipOver = imageURL.find("/", 8);
	std::string baseURL = imageURL.substr(8, skipOver - 8);
	std::string pageLink = imageURL.substr(skipOver);

	sf::Http http;
	sf::Http::Request request(pageLink);
	sf::Http::Response image_res;

	http.setHost(baseURL);
	image_res = http.sendRequest(request);

	//writing image to a file
	const std::string& body = image_res.getBody();
	std::ofstream fileImage("Images/book"+std::to_string(index)+".jpg", std::ios::out | std::ios::binary);
	fileImage.write(body.c_str(), body.size());
	fileImage.close();
}

void MainMenuState::SetUserId(const std::string& userId)
{
	this->userId = userId;
}

std::vector<float> MainMenuState::ConvertRating(float rating)
{
	std::vector<float> ratingNum = { 0 , 0 , 0 , 0 , 0 };
	for (int i = 0; i < 5; i++)
	{
		if (rating >= 1)
		{
			ratingNum[i] = 1;
			rating--;
		}
		else if (rating > 0.75)
		{
			ratingNum[i] = 1;
			break;
		}
		else if (rating > 0.25 && rating < 0.75)
		{
			ratingNum[i] = 0.5;
			break;
		}
	}
	return ratingNum;
}

void MainMenuState::UpdateBooks()
{
	messageSent = SaveEventIdInJson("0").dump();
	connectSocket->Send(messageSent.c_str(), messageSent.size());

	connectSocket->Receive(receivedBuffer.data(), receivedBuffer.size(), received);
	std::string messageReceived(receivedBuffer.data(), received);

	json j = json::parse(messageReceived);
	for (int i = 0; i < j.size(); i++)
	{
		Book book(j, i);
		books.push_back(book);
	}
}
