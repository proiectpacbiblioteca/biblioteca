#pragma once
#include <SFML/Network.hpp>
#include "State.h"
#include "Textbox.h"
#include "PushButton.h"
#include "Book.h"
#include <iostream>
#include <fstream>
#include <vector>

class MainMenuState :
    public State
{
private:
	//General Variables
	int pageNumber;
	int selectedBookIndex;
	bool profileButtonPressed;
	bool disconnectButtonPressed;
	bool borrowButtonPressed;
	int searchBarEmpty;
	std::vector<Book> books;
	std::string userId;
	sf::Clock clock;
	std::string borrowMessage;

	//Text Variables
	sf::Font fontAllura;
	sf::Font fontArial;
	sf::Text pageNumberText;
	sf::Text popUpText;

	//Visual Variables
	sf::RectangleShape booksBox;
	std::vector<PushButton> buttonVector;
	std::vector<sf::Sprite> starRating;
	sf::RectangleShape popUp;

	//Button Variables
	PushButton leftButton;
	PushButton rightButton;
	PushButton searchButtonName;
	PushButton searchButtonAuthor;
	PushButton searchButtonISBN;
	PushButton disconnectButton;
	PushButton profileButton;
	PushButton borrowButton;

	//Textbox variables
	Textbox searchBar;

	//Texture Variables
	sf::Texture cornerTexture;
	sf::Sprite corner1;
	sf::Sprite corner2;
	sf::Texture detailTexture;
	sf::Sprite detail;
	sf::Texture arrowTexture;
	sf::Sprite arrowLeft;
	sf::Sprite arrowRight;
	sf::Texture fullStar;
	sf::Texture halfStar;
	sf::Texture emptyStar;

	//Variables needed for client-servers
	std::string messageSent;
	std::array<char, 8192> receivedBuffer;
	int received;

public:
	//Constructor
	MainMenuState(sf::RenderWindow* window);

	//Polling
	virtual void PollEvents();

	//Initializers
	virtual void InitVariables() override;
	virtual void InitWindow() override;
	virtual void InitText() override;
	virtual void InitButtons() override;
	virtual void InitVisuals() override;

	//Renderers
	virtual void RenderVisuals(sf::Window& window) override;
	virtual void RenderText(sf::Window& window) override;
	virtual void RenderButtons(sf::Window& window) override;
	virtual void RenderBooks(sf::Window& window);

	//Other Methods
	void DownloadHttp(std::string, int);
	bool IsDisconnectButtonPressed();
	bool IsProfileButtonPressed();
	std::vector<float> ConvertRating(float);
	void UpdateBooks();
	void SetUserId(const std::string& userId);
};