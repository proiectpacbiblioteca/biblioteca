#pragma once
#include <string>
#include "../NetworkingLibrary/TcpSocket.h"

class Book
{
private:
	std::string bookId;
	std::string isbn;
	std::string authors;
	std::string originalPublicationYear;
	std::string title;
	std::string languageCode;
	std::string average_rating;
	std::string imageUrl;
	std::string smallImageUrl;
	std::string content;
public:
	Book(const json& bookInfo, int bookIndex);
	std::string GetBookId();
	std::string GetTitle();
	std::string GetAuthor();
	std::string GetISBN();
	std::string GetImageURL();
	std::string GetRating();
};

