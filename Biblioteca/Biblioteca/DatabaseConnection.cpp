#include "DatabaseConnection.h"

using json = nlohmann::json;

DatabaseConnection::DatabaseConnection(const char* path)
{
	dir = path;
	exit = sqlite3_open(dir, &DB);
}

DatabaseConnection::~DatabaseConnection()
{
	sqlite3_close(DB);
}

int DatabaseConnection::GetResultCallback(void* var, int count, char** data, char** columns)
{

	std::string* result = static_cast<std::string*> (var);

	result->push_back(**data);

	return 0;
}

int DatabaseConnection::GetDataCallback(void* var, int count, char** data, char** columns)
{

	std::vector<std::string>* bookData = static_cast<std::vector<std::string>*> (var);

	for (int index = 0; index < count; index++)
	{
		bookData->push_back(data[index]);
	}

	return 0;
}

std::string DatabaseConnection::SignUpUser(const std::string& username)
{

	std::string query = "SELECT EXISTS (SELECT 1 FROM Users WHERE username = '" + username + "')";

	std::string userExist;

	sqlite3_exec(DB, query.c_str(), GetResultCallback, &userExist, NULL);
	
	if (userExist == "1")
		return "denied";

	query = "INSERT INTO Users(username) "
	"VALUES('" + username + "')";

	sqlite3_exec(DB, query.c_str(), NULL, 0, NULL);

	return "accepted";
}

std::string DatabaseConnection::SignInUser(const std::string& username)
{
	std::string canSignIn;

	std::string query = "SELECT EXISTS "
		"(SELECT 1 FROM Users "
		"WHERE username = '" + username + "')";

	sqlite3_exec(DB, query.c_str(), GetResultCallback, &canSignIn, NULL);

	if (canSignIn == "0")
		return "denied";
	else
		return "accepted";
}

std::string DatabaseConnection::GetUserId(const std::string& username)
{
	std::vector<std::string> userId;

	std::string query = "SELECT user_id FROM Users WHERE username = '" + username + "'";

	sqlite3_exec(DB, query.c_str(), GetDataCallback, &userId, NULL);

	return userId[0];
}

std::string DatabaseConnection::DeleteUser(const std::string& userId)
{
	std::string hasLoanedBooks;

	std::string query = "SELECT EXISTS "
		"(SELECT 1 FROM UserBooks "
		"WHERE user_id = " + userId + ")";

	sqlite3_exec(DB, query.c_str(), GetResultCallback, &hasLoanedBooks, NULL);

	if (hasLoanedBooks == "1")
		return "denied";

	query = "DELETE FROM Users "
		"WHERE user_id = " + userId;

	sqlite3_exec(DB, query.c_str(), NULL, 0, NULL);

	return "accepted";

}

std::string DatabaseConnection::SearchBookByTitle(const std::string& title)
{
	std::vector<std::string> booksTitles;

	std::string query = "SELECT title FROM Books";

	sqlite3_exec(DB, query.c_str(), GetDataCallback, &booksTitles, NULL);

	std::string targetTitle;
	int editDistance;

	editDistance = LevenshteinDistance(title, booksTitles[0]);
	targetTitle = booksTitles[0];

	for (int index = 1; index < booksTitles.size(); index++)
	{
		int currentEditDistance;
		currentEditDistance = LevenshteinDistance(title, booksTitles[index]);

		if (editDistance > currentEditDistance)
		{
			editDistance = currentEditDistance;
			targetTitle = booksTitles[index];
		}

		if (editDistance == 0)
			break;
	}

	json bookInfo;

	std::vector<std::string> bookData;

	std::string query2 = "SELECT * FROM Books WHERE title = '" + targetTitle + "'";

	sqlite3_exec(DB, query2.c_str(), GetDataCallback, &bookData, NULL);

	for (int index = 0; index < bookData.size(); index++)
	{
		bookInfo["book_id"] = bookData[0];
		bookInfo["isbn"] = bookData[2];
		bookInfo["authors"] = bookData[3];
		bookInfo["original_publication_year"] = bookData[4];
		bookInfo["title"] = bookData[5];
		bookInfo["language_code"] = bookData[6];
		bookInfo["average_rating"] = bookData[7];
		bookInfo["image_url"] = bookData[8];
		bookInfo["small_image_url"] = bookData[9];
		bookInfo["content"] = bookData[10];
	}

	std::string bookInfoString = bookInfo.dump();
	return bookInfoString;
}

std::string DatabaseConnection::SearchBookByAuthor(const std::string& author)
{
	std::vector<std::string> booksAuthors;

	std::string query = "SELECT authors FROM Books";

	sqlite3_exec(DB, query.c_str(), GetDataCallback, &booksAuthors, NULL);

	std::string targetAuthor;
	int editDistance;

	editDistance = LevenshteinDistance(author, booksAuthors[0]);
	targetAuthor = booksAuthors[0];

	for (int index = 1; index < booksAuthors.size(); index++)
	{
		int currentEditDistance;
		currentEditDistance = LevenshteinDistance(author, booksAuthors[index]);

		if (editDistance > currentEditDistance)
		{
			editDistance = currentEditDistance;
			targetAuthor = booksAuthors[index];
		}

		if (editDistance == 0)
			break;
	}

	json bookInfo;

	std::vector<std::string> bookData;

	std::string query2 = "SELECT * FROM Books WHERE authors = '" + targetAuthor + "'";

	sqlite3_exec(DB, query2.c_str(), GetDataCallback, &bookData, NULL);

	for (int index = 0; index < bookData.size(); index++)
	{
		bookInfo["book_id"] = bookData[0];
		bookInfo["isbn"] = bookData[2];
		bookInfo["authors"] = bookData[3];
		bookInfo["original_publication_year"] = bookData[4];
		bookInfo["title"] = bookData[5];
		bookInfo["language_code"] = bookData[6];
		bookInfo["average_rating"] = bookData[7];
		bookInfo["image_url"] = bookData[8];
		bookInfo["small_image_url"] = bookData[9];
		bookInfo["content"] = bookData[10];
	}

	std::string bookInfoString = bookInfo.dump();
	return bookInfoString;
}

std::string DatabaseConnection::SearchBookByIsbn(const std::string& isbn)
{
	std::vector<std::string> bookData;

	std::string query = "SELECT * FROM Books "
		"WHERE isbn = '" + isbn + "'";

	sqlite3_exec(DB, query.c_str(), GetDataCallback, &bookData, NULL);

	json bookInfo;

	for (int index = 0; index < bookData.size(); index++)
	{
		bookInfo["book_id"] = bookData[0];
		bookInfo["isbn"] = bookData[2];
		bookInfo["authors"] = bookData[3];
		bookInfo["original_publication_year"] = bookData[4];
		bookInfo["title"] = bookData[5];
		bookInfo["language_code"] = bookData[6];
		bookInfo["average_rating"] = bookData[7];
		bookInfo["image_url"] = bookData[8];
		bookInfo["small_image_url"] = bookData[9];
		bookInfo["content"] = bookData[10];
	}

	std::string bookInfoString = bookInfo.dump();
	return bookInfoString;
}

int DatabaseConnection::LevenshteinDistance(const std::string& source, const std::string& target)
{

	std::vector<std::vector<int>> table(target.size() + 1, std::vector<int>(source.size() + 1));;

	for (int indexRow = 0; indexRow < table.size(); indexRow++)
	{
		table[indexRow][0] = indexRow;
	}

	for (int indexColumn = 1; indexColumn < table[0].size(); indexColumn++)
	{
		table[0][indexColumn] = indexColumn;
	}

	for (int indexRow = 1; indexRow < table.size(); indexRow++)
	{
		for (int indexColumn = 1; indexColumn < table[0].size(); indexColumn++)
		{
			if (source[indexColumn - 1] == target[indexRow - 1])
			{
				table[indexRow][indexColumn] = table[indexRow - 1][indexColumn - 1];
			}
			else
			{
				table[indexRow][indexColumn] = std::min({
										table[indexRow - 1][indexColumn - 1],
										table[indexRow - 1][indexColumn],
										table[indexRow][indexColumn - 1] }) + 1;
			}
		}
	}
	return table[table.size() - 1][table[0].size() - 1];
}

bool DatabaseConnection::CanUserLoanBook(const std::string& userId, const std::string& bookId)
{
	if (CheckIfUserLoanedMoreThanFourBooks(userId) == true)
		return false;

	if (CheckIfUserReturnedBooksInTime(userId) == false)
		return false;

	if (CheckIfBookAvailable(bookId) == false)
		return false;

	if (CheckIfUserAlreadyHasTheBook(userId, bookId) == true)
		return false;

	return true;
}

bool DatabaseConnection::CheckIfUserLoanedMoreThanFourBooks(const std::string& userId)
{
	std::string hasMoreThanFourBooks;

	std::string query = "SELECT IIF "
		"((SELECT COUNT(*) "
		"FROM UserBooks WHERE user_id = " + userId + ") > 4, 1, 0) Result";

	sqlite3_exec(DB, query.c_str(), GetResultCallback, &hasMoreThanFourBooks, NULL);

	if (hasMoreThanFourBooks == "1")
		return true;
	else
		return false;
}

bool DatabaseConnection::CheckIfUserReturnedBooksInTime(const std::string& userId)
{
	std::string returnedBooksInTime;

	std::string query = "SELECT IIF "
		"((SELECT COUNT(*) FROM UserBooks "
		"WHERE user_id = " + userId + " AND return_date < DATE('now')) > 0, 0, 1) Result";

	sqlite3_exec(DB, query.c_str(), GetResultCallback, &returnedBooksInTime, NULL);

	if (returnedBooksInTime == "1")
		return true;
	else
		return false;
}

bool DatabaseConnection::CheckIfBookAvailable(const std::string& bookId)
{
	std::string bookAvailable;

	std::string query = "SELECT IIF "
		"((SELECT books_count "
		"FROM Books WHERE book_id = " + bookId + ") > 0, 1, 0) Result";

	sqlite3_exec(DB, query.c_str(), GetResultCallback, &bookAvailable, NULL);

	if (bookAvailable == "1")
		return true;
	else
		return false;
}

bool DatabaseConnection::CheckIfUserAlreadyHasTheBook(const std::string& userId, const std::string& bookId)
{
	std::string hasTheBook;

	std::string query = "SELECT IIF "
		"((SELECT COUNT(*) FROM UserBooks "
		"WHERE user_id = " + userId + " AND book_id = " + bookId + ") = 1, 1, 0) Result";

	sqlite3_exec(DB, query.c_str(), GetResultCallback, &hasTheBook, NULL);

	if (hasTheBook == "1")
		return true;
	else
		return false;
}

std::string DatabaseConnection::LoanBook(const std::string& userId, const std::string& bookId)
{
	if (CanUserLoanBook(userId, bookId) == false)
		return "denied";

	std::string query = "INSERT INTO UserBooks(user_id, book_id) "
		"VALUES(" + userId + ", " + bookId + ")";

	sqlite3_exec(DB, query.c_str(), NULL, 0, NULL);

	DecreaseBooksCount(bookId);

	return "accepted";
}

std::string DatabaseConnection::ReturnBook(const std::string& userId, const std::string& bookId)
{
	std::string query = "DELETE FROM UserBooks "
		"WHERE user_id = " + userId + " AND book_id = " + bookId;

	sqlite3_exec(DB, query.c_str(), NULL, 0, NULL);

	IncreaseBooksCount(bookId);

	return "accepted";
}

void DatabaseConnection::IncreaseBooksCount(const std::string& bookId)
{

	std::string query = "UPDATE Books "
		"SET books_count = books_count + 1 "
		"WHERE book_id = " + bookId;

	sqlite3_exec(DB, query.c_str(), NULL, 0, NULL);

}

void DatabaseConnection::DecreaseBooksCount(const std::string& bookId)
{

	std::string query = "UPDATE Books "
		"SET books_count = books_count - 1 "
		"WHERE book_id = " + bookId;

	sqlite3_exec(DB, query.c_str(), NULL, 0, NULL);

}

bool DatabaseConnection::CanUserExtendLoanTime(const std::string& userId, const std::string& bookId)
{
	std::string canExtend;

	std::string query = "SELECT IIF "
		"((SELECT times_extended FROM UserBooks "
		"WHERE user_id = " + userId + " AND book_id = " + bookId + ") < 2, 1, 0) Result";

	sqlite3_exec(DB, query.c_str(), GetResultCallback, &canExtend, NULL);

	if (canExtend == "1")
		return true;
	else
		return false;
}

std::string DatabaseConnection::ExtendLoan(const std::string& userId, const std::string& bookId)
{
	if (CanUserExtendLoanTime(userId, bookId) == false)
		return "denied";

	std::string query = "UPDATE UserBooks "
		"SET return_date = DATE(return_date, '+3 day'), times_extended = times_extended + 1 "
		"WHERE user_id = " + userId + " AND book_id = " + bookId;

	sqlite3_exec(DB, query.c_str(), NULL, 0, NULL);

	return "accepted";

}

std::string DatabaseConnection::GetUserBooks(const std::string& userId)
{
	std::vector<std::string> booksId;

	std::string query = "SELECT book_id FROM UserBooks WHERE user_id = " + userId;

	sqlite3_exec(DB, query.c_str(), GetDataCallback, &booksId, NULL);

	json bookInfo;
	
	for (int indexBooksId = 0; indexBooksId < booksId.size(); indexBooksId++)
	{
		std::vector<std::string> bookData;

		query = "SELECT * FROM Books WHERE book_id = " + booksId[indexBooksId];

		sqlite3_exec(DB, query.c_str(), GetDataCallback, &bookData, NULL);

		bookInfo["Book" + std::to_string(indexBooksId)]["book_id"] = bookData[0];
		bookInfo["Book" + std::to_string(indexBooksId)]["isbn"] = bookData[2];
		bookInfo["Book" + std::to_string(indexBooksId)]["authors"] = bookData[3];
		bookInfo["Book" + std::to_string(indexBooksId)]["original_publication_year"] = bookData[4];
		bookInfo["Book" + std::to_string(indexBooksId)]["title"] = bookData[5];
		bookInfo["Book" + std::to_string(indexBooksId)]["language_code"] = bookData[6];
		bookInfo["Book" + std::to_string(indexBooksId)]["average_rating"] = bookData[7];
		bookInfo["Book" + std::to_string(indexBooksId)]["image_url"] = bookData[8];
		bookInfo["Book" + std::to_string(indexBooksId)]["small_image_url"] = bookData[9];
		bookInfo["Book" + std::to_string(indexBooksId)]["content"] = bookData[10];
	}

	std::string bookInfoString = bookInfo.dump();
	return bookInfoString;
}

std::string DatabaseConnection::GetSixRandomBooks()
{

	json bookInfo;

	for (int indexBooksId = 0; indexBooksId < 6; indexBooksId++)
	{
		std::vector<std::string> bookData;

		std::string query = "SELECT * FROM Books WHERE book_id = abs(random() % 1000) + 1";

		sqlite3_exec(DB, query.c_str(), GetDataCallback, &bookData, NULL);

		bookInfo["Book" + std::to_string(indexBooksId)]["book_id"] = bookData[0];
		bookInfo["Book" + std::to_string(indexBooksId)]["isbn"] = bookData[2];
		bookInfo["Book" + std::to_string(indexBooksId)]["authors"] = bookData[3];
		bookInfo["Book" + std::to_string(indexBooksId)]["original_publication_year"] = bookData[4];
		bookInfo["Book" + std::to_string(indexBooksId)]["title"] = bookData[5];
		bookInfo["Book" + std::to_string(indexBooksId)]["language_code"] = bookData[6];
		bookInfo["Book" + std::to_string(indexBooksId)]["average_rating"] = bookData[7];
		bookInfo["Book" + std::to_string(indexBooksId)]["image_url"] = bookData[8];
		bookInfo["Book" + std::to_string(indexBooksId)]["small_image_url"] = bookData[9];
		bookInfo["Book" + std::to_string(indexBooksId)]["content"] = bookData[10];
	}

	std::string bookInfoString = bookInfo.dump();
	return bookInfoString;
}
