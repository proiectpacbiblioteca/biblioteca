#include "DisplayingMessages.h"

void DisplayingMessages::InputProvidedIsWrong()
{
	std::cout << "The input provided is wrong. ";
	std::cout << std::endl;
}

void DisplayingMessages::TooManyBorrowedBooks()
{
	std::cout << "Too many books were borrowed. ";
	std::cout << std::endl;
}

void DisplayingMessages::BorrowingButNotReturning()
{
	std::cout << "The borrowed books were not returned in time. ";
	std::cout << std::endl;
}

void DisplayingMessages::AlreadyBorrowedThisBook()
{
	std::cout << "You have already borrowed this book. ";
		std::cout << std::endl;
}

void DisplayingMessages::UnavailableBook()
{
	std::cout << "All the books have been borrowed. ";
	std::cout << std::endl;
}
