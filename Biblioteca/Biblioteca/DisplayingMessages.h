#pragma once
#include <iostream>
#include <string>

class DisplayingMessages{

public:
	static void InputProvidedIsWrong();
	static void TooManyBorrowedBooks();
	static void BorrowingButNotReturning();
	static void AlreadyBorrowedThisBook();
	static void UnavailableBook();
};