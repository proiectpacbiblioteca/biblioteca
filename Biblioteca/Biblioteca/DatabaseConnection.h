#pragma once
#include <string>
#include <sqlite3.h>
#include "json.hpp"

using json = nlohmann::json;

class DatabaseConnection
{
private:
	const char* dir;
	sqlite3* DB;
	int exit;
	static int GetResultCallback(void* var, int count, char** data, char** columns);
	static int GetDataCallback(void* var, int count, char** data, char** columns);
public:
	DatabaseConnection(const char* path);
	~DatabaseConnection();

	std::string SignUpUser(const std::string& username);
	std::string SignInUser(const std::string& username);
	std::string GetUserId(const std::string& username);
	std::string DeleteUser(const std::string& userId);

	std::string SearchBookByTitle(const std::string& title);
	std::string SearchBookByAuthor(const std::string& author);
	std::string SearchBookByIsbn(const std::string& isbn);

	int LevenshteinDistance(const std::string& source, const std::string& target);

	bool CanUserLoanBook(const std::string& userId, const std::string& bookId);
	bool CheckIfUserLoanedMoreThanFourBooks(const std::string& userId);
	bool CheckIfUserReturnedBooksInTime(const std::string& userId);
	bool CheckIfBookAvailable(const std::string& bookId);
	bool CheckIfUserAlreadyHasTheBook(const std::string& userId, const std::string& bookId);

	std::string LoanBook(const std::string& userId, const std::string& bookId);
	std::string ReturnBook(const std::string& userId, const std::string& bookId);

	void IncreaseBooksCount(const std::string& bookId);
	void DecreaseBooksCount(const std::string& bookId);

	bool CanUserExtendLoanTime(const std::string& userId, const std::string& bookId);
	std::string ExtendLoan(const std::string& userId, const std::string& bookId);

	std::string GetUserBooks(const std::string& userId);
	std::string GetSixRandomBooks();
};