#pragma once
#include <iostream>
#include <iomanip>
#include <ctime>

class Logger
{
public:
	Logger();
	void Log(const std::string& message);
};

