#include "Logger.h"

Logger::Logger()
{
}

void Logger::Log(const std::string& message)
{
	std::time_t crtTime = std::time(nullptr);
	std::cout << '[' << std::put_time(std::localtime(&crtTime), "%Y-%m-%d %H:%M:%S") << ']';
	std::cout << ' ' << "[INFO]: " << ' ' << message << '\n';
}
