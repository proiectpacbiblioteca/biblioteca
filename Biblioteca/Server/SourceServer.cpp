#include "SourceServer.h"

void Server::SetDatabase(const std::string& path)
{
	context = new DatabaseConnection(path.c_str());
}

enum class Events
{
	GetSixRandomBooks,
	SignUp,
	SignIn,
	GetUserId,
	DeleteUser,
	SearchBookByTitle,
	SearchBookByAuthor,
	SerachBookByIsbn,
	GetUserBooks,
	LoanBook,
	ReturnBook,
	ExtendLoan
};

std::string GetSixRandomBooks()
{
	return Server::context->GetSixRandomBooks();
}

std::string SignUpUser(const std::string& username)
{
	std::string response = Server::context->SignUpUser(username);
	if (response == "accepted")
		Server::logger.Log("Client signed up");
	else
		Server::logger.Log("Client cound not sign up");
	return response;
}

std::string SignInUser(const std::string& username)
{
	std::string response = Server::context->SignInUser(username);
	if (response == "accepted")
		Server::logger.Log("Client singed in");
	else
		Server::logger.Log("Client cound not sign in");
	return response;
}

std::string GetUserId(const std::string& username)
{
	return Server::context->GetUserId(username);
}

std::string DeleteUser(const std::string& userId)
{
	return Server::context->DeleteUser(userId);
}

std::string SearchBookByTitle(const std::string& title)
{
	return Server::context->SearchBookByTitle(title);
}

std::string SearchBookByAuthor(const std::string& author)
{
	return Server::context->SearchBookByAuthor(author);
}

std::string SearchBookByIsbn(const std::string& isbn)
{
	return Server::context->SearchBookByIsbn(isbn);
}

std::string GetUserBooks(const std::string& userId)
{
	return Server::context->GetUserBooks(userId);
}

std::string LoanBook(const std::string& userId, const std::string& bookId)
{
	return Server::context->LoanBook(userId, bookId);
}

std::string ReturnBook(const std::string& userId, const std::string& bookId)
{
	std::string response = Server::context->ReturnBook(userId, bookId);
	if (response == "accepted")
		Server::logger.Log("Return book approved");
	else
		Server::logger.Log("Return book denied");
	return response;
}

std::string ExtendLoan(const std::string& userId, const std::string& bookId)
{
	
	std::string response = Server::context->ExtendLoan(userId, bookId);
	if(response == "accepted")
		Server::logger.Log("Extend loan approved");
	else
		Server::logger.Log("Extend loan denied");
	return response;
}

void MapEvents(std::unordered_map<Events, std::any>& myMap)
{
	myMap[Events::GetSixRandomBooks] = GetSixRandomBooks;
	myMap[Events::SignUp] = SignUpUser;
	myMap[Events::SignIn] = SignInUser;
	myMap[Events::GetUserId] = GetUserId;
	myMap[Events::DeleteUser] = DeleteUser;
	myMap[Events::SearchBookByTitle] = SearchBookByTitle;
	myMap[Events::SearchBookByAuthor] = SearchBookByAuthor;
	myMap[Events::SerachBookByIsbn] = SearchBookByIsbn;
	myMap[Events::GetUserBooks] = GetUserBooks;
	myMap[Events::LoanBook] = LoanBook;
	myMap[Events::ReturnBook] = ReturnBook;
	myMap[Events::ExtendLoan] = ExtendLoan;
}

int main()
{
	std::unordered_map<Events, std::any> myMap;
	Server::SetDatabase("..\\Library\\Library.db");
	MapEvents(myMap);
	Server::logger.Log("Starting server...");

	TcpSocket listener;
	listener.Listen(27015);

	Server::logger.Log("Waiting for client to connect...");
	TcpSocket client = listener.Accept();
	Server::logger.Log("Client connected");

	while (true)
	{
		try {
			// receive
			std::array<char, 8192> receivedBuffer;
			int received;
			client.Receive(receivedBuffer.data(), receivedBuffer.size(), received);

			std::string receivedString(receivedBuffer.data(), received);
			if (receivedString != "")
			{

				Server::logger.Log("Received:" + receivedString);

				// send
				json j = json::parse(receivedString);
				std::string eventIdAsString = j["event"];
				int eventId = atoi(eventIdAsString.c_str());
				auto it = myMap[static_cast<Events>(eventId)];
				std::string message;
				if (eventId == 0)
				{
					message = std::any_cast<std::string(*) ()> (it) ();
				}
				else if (eventId > 0 && eventId < 9)
				{
					message = std::any_cast<std::string(*) (const std::string&)> (it) (j["data1"]);
				}
				else if (eventId > 8 && eventId < 12)
				{
					message = std::any_cast<std::string(*) (const std::string&, const std::string&)> (it) (j["data1"], j["data2"]);
				}

				client.Send(message.c_str(), message.size());
			}
		}
		catch (const std::string& errorMessage)
		{
			Server::logger.Log("Error occured! Message received: " + errorMessage);
		}
	}

	std::cout << "Closing server. Bye!" << std::endl;

	return 0;
}
