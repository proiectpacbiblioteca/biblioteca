#pragma once
#include <iostream>
#include <array>
#include <sstream>
#include <unordered_map>
#include "../NetworkingLibrary/TcpSocket.h"
#include "../Biblioteca/DatabaseConnection.h"
#include "../Logging/Logger.h"
#include <any>

static class Server
{
public:
	inline static DatabaseConnection* context;
	inline static Logger logger;
	static void SetDatabase(const std::string&);
};